import tensorflow as tf


class MyModel(tf.keras.Model):

    def __init__(self):
        super(MyModel, self).__init__()
        self.step = -1

        # first block
        self.noise = tf.keras.layers.GaussianNoise(0.01)
        self.conv1 = tf.keras.layers.Conv2D(32, 3, activation='elu')
        self.bn1 = tf.keras.layers.BatchNormalization(axis=3)
        self.mp1 = tf.keras.layers.MaxPool2D(2)

        # 32s
        self.conv2 = tf.keras.layers.Conv2D(32, 3, activation='elu', padding="same")
        self.bn2 = tf.keras.layers.BatchNormalization(axis=3)
        self.conv3 = tf.keras.layers.Conv2D(32, 3, activation='elu', padding="same")
        self.bn3 = tf.keras.layers.BatchNormalization(axis=3)

        self.conv4 = tf.keras.layers.Conv2D(32, 3, activation='elu', padding="same")
        self.bn4 = tf.keras.layers.BatchNormalization(axis=3)
        self.conv5 = tf.keras.layers.Conv2D(32, 3, activation='elu', padding="same")
        self.bn5 = tf.keras.layers.BatchNormalization(axis=3)

        # 64s
        self.identityConv64 = tf.keras.layers.Conv2D(64, 1, activation=None)
        self.conv6 = tf.keras.layers.Conv2D(64, 3, activation='elu', padding="same")
        self.bn6 = tf.keras.layers.BatchNormalization(axis=3)
        self.conv7 = tf.keras.layers.Conv2D(64, 3, activation='elu', padding="same")
        self.bn7 = tf.keras.layers.BatchNormalization(axis=3)

        # output
        self.outputAveragePool = tf.keras.layers.GlobalAveragePooling2D()
        self.outputDense = tf.keras.layers.Dense(100, activation='tanh')
        self.outputDo = tf.keras.layers.Dropout(0.5)
        self.outputSoftmax = tf.keras.layers.Dense(2, activation='softmax')

    def call(self, inputs, training):
        self.step = self.step + 1
        print("step: ", self.step)

        # first block
        x = inputs
        if self.step % 5000 == 0:
            tf.summary.image("raw_image", x, max_outputs=1, step=self.step)
        x = self.noise(x)
        x = self.conv1(x)
        x = self.bn1(x)
        if self.step % 5000 == 0:
            bn_image = tf.slice(x, [0, 0, 0, 0], [-1, -1, -1, 1])
            tf.summary.image("bn_image1", bn_image, max_outputs=1, step=self.step)
            bn_image = tf.slice(x, [0, 0, 0, 1], [-1, -1, -1, 1])
            tf.summary.image("bn_image2", bn_image, max_outputs=1, step=self.step)
            bn_image = tf.slice(x, [0, 0, 0, 2], [-1, -1, -1, 1])
            tf.summary.image("bn_image3", bn_image, max_outputs=1, step=self.step)
        x = self.mp1(x)

        # 32s
        y = x
        x = self.conv2(x)
        x = self.bn2(x)
        x = self.conv3(x)
        x = self.bn3(x)
        x = tf.keras.layers.add([x, y])
        y = x
        x = self.conv4(x)
        x = self.bn4(x)
        x = self.conv5(x)
        x = self.bn5(x)
        x = tf.keras.layers.add([x, y])

        # 64s
        y = x
        y = self.identityConv64(y)
        x = self.conv6(x)
        x = self.bn6(x)
        x = self.conv7(x)
        x = self.bn7(x)
        x = tf.keras.layers.add([x, y])

        #output
        x = self.outputAveragePool(x)
        x = self.outputDense(x)
        x = self.outputDo(x, training=training)
        output = self.outputSoftmax(x)

        return output
