import tensorflow as tf
from offsite.model import MyModel
import tensorflow_datasets as tfds

print(tf.__version__)

def format_example(image, label):
    image = tf.cast(image, tf.float32)
    image = (image/127.5) - 1
    image = tf.image.resize(image, (160, 160))
    return image, label

SPLIT_WEIGHTS = (8, 1, 1)
splits = tfds.Split.TRAIN.subsplit(weighted=SPLIT_WEIGHTS)
(raw_train, raw_validation, raw_test), metadata = tfds.load('cats_vs_dogs', split=list(splits), with_info=True, as_supervised=True)
train = raw_train.map(format_example)
validation = raw_validation.map(format_example)
test = raw_test.map(format_example)

train_batches = train.shuffle(1024).batch(32)
validation_batches = validation.batch(32)

model = MyModel()

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="/opt/tensorboard")
summary_writer = tf.summary.create_file_writer('/opt/tensorboard')
with summary_writer.as_default():
    model.fit(train_batches, epochs=10, validation_data=validation_batches)

